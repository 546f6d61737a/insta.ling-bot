$(document).ready(function(){
	function updateChilds(){
		$("#childs").html("");
		getStorage().get('childs', function(result){
			if(result.childs){
				for(var child in result.childs)
					$("#childs").append('<option>'+child+'</option>');
			}
		});

	}
	$("#showWords").click(function() {
		getStorage().get('childs', function(result){
			if(result.childs){
				var id = parseInt($( "#childs option:selected" ).text())
				var words = result.childs[id];
				if(words){
					var csvContent = "";
					$.each(words, function(key, value){
						csvContent += '"' + value.word + '","' + value.translations + '","' + value.usageExample + '"\n';
					}); 
					download("child"+id+"_"+stringDate()+".csv",csvContent);
				}
			}
		});
	});
	$("#deleteWords").click(function() {
		getStorage().get('childs', function(result){
			if(result.childs){
				var id = parseInt($( "#childs option:selected" ).text())
				if(result.childs[id]){
					delete result.childs[id];
				}
				getStorage().set({'childs': result.childs}, updateChilds);
			}
		});
	});
	updateChilds();
	function download(filename, text) {
		var pom = document.createElement('a');
		pom.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
		pom.setAttribute('download', filename);

		if (document.createEvent) {
		    var event = document.createEvent('MouseEvents');
		    event.initEvent('click', true, true);
		    pom.dispatchEvent(event);
		}
		else {
		    pom.click();
		}
	}

	function stringDate(){
		var currentdate = new Date(); 
		return 		currentdate.getDate() + "-"
		            + (currentdate.getMonth()+1)  + "-" 
		            + currentdate.getFullYear() + "_"  
		            + currentdate.getHours() + "-"  
		            + currentdate.getMinutes() + "-" 
		            + currentdate.getSeconds();
	}
	var _check_min = $("#check_min");
	var _check_max = $("#check_max");
	var _nextword_min = $("#nextword_min");
	var _nextword_max = $("#nextword_max");
	var _autocomplete = $("#autocomplete");
	var _audio_translation = $("#audio_translation");
	var _automode = $("#automode");
	var _grundformen = $("#grundformen");

	function setValues(){
		_check_max.prop('min', parseInt(_check_min.val()) +1);
		_nextword_max.prop('min', parseInt(_nextword_min.val()) +1);
	}

	function loadValues(){
		getStorage().get('settings',function(result) {
			if(result.settings){
				var s = result.settings;
				_check_min.val(s.check_min / 1000);
				_check_max.val(s.check_max / 1000);
				_nextword_min.val(s.nextword_min / 1000);
				_nextword_max.val(s.nextword_max / 1000);
				_autocomplete.prop('checked', s.mode & 0x01);
				_audio_translation.prop('checked', s.mode & 0x02);
				_automode.prop('checked', s.mode & 0x04);
				_grundformen.prop('checked', s.mode & 0x08);
			}
		});
		setValues();

	}

	function saveValues(){
		if(parseInt(_check_min.val()) >= parseInt(_check_max.val())){
			_check_max.val((parseInt(_check_min.val()) +1).toString());
		}
		if(parseInt(_nextword_min.val()) >= parseInt(_nextword_max.val())){
			_nextword_max.val((parseInt(_nextword_min.val()) +1).toString());
		}
		getStorage().set({'settings': {
				check_min: _check_min.val() * 1000,
				check_max: _check_max.val() * 1000,
				nextword_min: _nextword_min.val() * 1000,
				nextword_max: _nextword_max.val() * 1000,
				mode: (_autocomplete.is(':checked') ? 0x01 : 0) |
					(_audio_translation.is(':checked') ? 0x02 : 0) |
					(_automode.is(':checked') ? 0x04 : 0) |
					(_grundformen.is(':checked') ? 0x08 : 0)
		}});
		setValues();
	}

	loadValues();
	// _mode
	// 0x01 - autocomplete
	// 0x02 - audio translation
	// 0x04 - automode
	// 0x08 - grundformen

	$( "input" ).change(saveValues);
});
