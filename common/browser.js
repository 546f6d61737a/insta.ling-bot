function getStorage(){
    switch(_storage_code){
        case 0:
            return chrome.storage.local;
        case 1:
            return chrome.storage.sync;
        case 2:
            return chrome.storage.managed;
    }
}

getStorage().get('settings',function(result) {
    if(!result.settings){
        getStorage().set({'settings': {
            check_min: _check_minTimeout,
            check_max: _check_maxTimeout,
            nextword_min: _nextWord_minTimeout,
            nextword_max: _nextWord_maxTimeout,
            mode: _mode
        }});
    }
}); 
