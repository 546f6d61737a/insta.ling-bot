function injectJQuery(cb){
	injectScript('common/jquery-3.1.1.min.js',cb);
}

function injectScript(path, cb){
	var script = document.createElement('script');
	script.type = 'text/javascript';
	script.src = chrome.extension.getURL(path);
	script.onload = function() {
   		this.remove();
		if (typeof cb === 'function')
			cb();	
	};
	(document.head||document.documentElement).appendChild(script);
}

function injectCode(code){
	var script = document.createElement('script');
	script.type = 'text/javascript';
	script.textContent = code;
	(document.head||document.documentElement).appendChild(script);
	script.remove();
}

function getDiff(old_string, new_string){
    var diff = new String();
        
    var s1 = old_string.toString().split(' ');
    var s2 = new_string.toString().split(' ');
  

    for(x = 0; x < s1.length; x++)
    {
        if(s2[x] != s1[x])
        {
            var w1 = s1[x];
            var w2 = s2[x];
            for(y = 0; y<w1.length; y++){
                if(w1[y] != w2[y])
                    diff += w2[y];
            }
            diff += ' ';
        }
    }

    return diff.trim();
}
