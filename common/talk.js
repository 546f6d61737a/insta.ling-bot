var _isPage = false;
function __initTalk(){
	$('body').append('<div style="display: none;" id="_hiddenDiv_"></div>');
	_isPage = true;
}

function __recv(cb){
	document.getElementById('_hiddenDiv_').addEventListener(_isPage ? 'content-page' : 'page-content', function() {
		if (typeof cb === 'function')
			cb(JSON.parse(document.getElementById('_hiddenDiv_').innerText));
	});
}

function __send(data) {
	var customEvent = document.createEvent('Event');
	customEvent.initEvent(_isPage ? 'page-content' : 'content-page', true, true);
	var hiddenDiv = document.getElementById('_hiddenDiv_');
	hiddenDiv.innerText = JSON.stringify(data);
	hiddenDiv.dispatchEvent(customEvent);
}
