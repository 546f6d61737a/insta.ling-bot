function _getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

function _getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function _findPattern(src,first,second,start = 0){
	var f = src.indexOf(first,start) + first.length;
	var s = src.indexOf(second,f);
	return src.substring(f,s);
}
