$(document).ready(function () {

	__initTalk();
	
	var learningPageShow_old = learningPageShow;
	var showAnswerPage_old = showAnswerPage;


    var isMarketing = false;
    var isNewWord = false;
	learningPageShow = function (id, speechPart, usageExample, translations, word, has_audio, audio_file_name, is_new_word, is_marketing) {
		learningPageShow_old.call(this, id, speechPart, usageExample, translations, word, has_audio, audio_file_name, is_new_word, is_marketing);

		__send({
			func: "learningPageShow",
			id: id,
			speechPart: speechPart,
			usageExample: usageExample,
			translations: translations,
			word: word,
			has_audio: has_audio,
			audio_file_name: audio_file_name,
			is_new_word: is_new_word,
			is_marketing: is_marketing
		});
	}

	showAnswerPage = function (id, answer, usageExample, translations, grade, word, answer_show, has_audio, show_grade) {
		showAnswerPage_old.call(this, id, answer, usageExample, translations, grade, word, answer_show, has_audio, show_grade);

		__send({
			func: "showAnswerPage",
			id: id,
			answer: answer,
			usageExample: usageExample,
			translations: translations,
			grade: grade,
			word: word,
			answer_show: answer_show,
			has_audio: has_audio,
			show_grade: show_grade
		});

		if (_mode & 0x04) {
			_timeoutClick($("#nextword"), _getRandomInt(_nextWord_minTimeout, _nextWord_maxTimeout));
		}

	}

	function __answer(answer, is_marketing, is_new_word){
        function _answer(answer){
            if(!$("#answer").is(':visible')) 
                return;
            $("#answer").val(answer);

            if (_mode & 0x04) 
                _timeoutClick($('#check'), _getRandomInt(_check_minTimeout, _check_maxTimeout));
        }
	
        if (_mode & 0x01) {

			if (!is_marketing && !is_new_word) {

				_answer(answer);
		
			} else if (_mode & 0x04) {

				_timeoutClick($('#know_new'),_getRandomInt(_check_minTimeout, _check_maxTimeout));

				$('#know_new').one('click',function(){

					if (is_marketing)
						_timeoutClick($('#skip'), _getRandomInt(_nextWord_minTimeout, _nextWord_maxTimeout));
					else if(!(_mode & 0x08))
						_answer(answer);

				});
			}
		}
    }
    

	
	function _timeoutClick(element, timeout, cb = null) {
		var t = setTimeout(function () {
			element.click();
			if (cb) cb();
			t = false;
		}, timeout);
		element.one("click",function () {
			if(t) clearTimeout(t);
		});
	}

	__recv(function (data) {
		switch (data.func) {
		case "setSettings":
			_check_minTimeout = data.check_min;
			_check_maxTimeout = data.check_max;
			_nextWord_minTimeout = data.nextword_min;
			_nextWord_maxTimeout = data.nextword_max;
			_mode = data.mode;
			break;
		case "getSettings":
			__send({
				func: data.func,
				check_min: _check_minTimeout,
				check_max: _check_maxTimeout,
				nextword_min: _nextWord_minTimeout,
				nextword_max: _nextWord_maxTimeout,
				mode: _mode
			});
			break;

        case "setAnswer":
            __answer(data.answer, data.is_marketing, data.is_new_word);
            break;
        }
        
	});

	$("#start_session_page").find("ul li:last-child").fadeOut(3000);
    $("#answer").off("paste");
    console.log("Załadowano!");
});
