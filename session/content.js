var instaling_version = '43yo4ihw';

addEventListener("load", windowLoaded, false);
var currentChildId = -1;
function windowLoaded() {
	var src = document.documentElement.innerHTML;
	currentChildId = parseInt(_findPattern(src,'var currentChildId = "', '";'));
	var instaling_readed_version = _findPattern(src,"updateParams(id, answer, true, '", "');");

	if(instaling_version != instaling_readed_version){
		alert("Wykryto nowa wersje instaling.pl");
	}
    injectScript("settings.js", function (){
        injectScript("common/util.js", function (){
            injectScript("common/talk.js", function () {
                injectScript("session/inject.js", onScriptInjected); 
            });
        });
    });
}

function onScriptInjected(){
	__recv(onReceive);
    loadSettings();
}

function onReceive(data){
	switch(data.func){
		case "learningPageShow":
            getStorage().get('settings',function(result) {
                if(result.settings){
                    var s = result.settings;

                        getStorage().get('childs', function(result){
                            var answer = (data.has_audio && (s.mode & 0x02)) ? decodeURIComponent(data.audio_file_name) : data.word;
                            if(s.mode & 0x08 && result.childs && result.childs[currentChildId] && result.childs[currentChildId][data.id]){
                                var word = result.childs[currentChildId][data.id].usageExample;
                                answer = getDiff( data.usageExample, word).toString()
                            } 
                            __send({ func: "setAnswer",
                                answer: answer,
                                is_marketing: data.is_marketing,
                                is_new_word: data.is_new_word
                            });
                        
                        });
                        
    
                }
            });
			break;
		case "showAnswerPage":
			getStorage().get('childs', function(result){
				var childs = result.childs;

				if(!result.childs)
					childs = new Object();

				if(!childs[currentChildId])
					childs[currentChildId] = new Object();	
			
				childs[currentChildId][data.id] = {
					usageExample: data.usageExample,
					translations: data.translations,
					word: data.word
				};

				getStorage().set({'childs': childs});
			});
			break;
	}
} 

function loadSettings(){
	getStorage().get('settings',function(result) {
		if(result.settings){
			var s = result.settings;
			__send({ func: "setSettings",
				check_min: s.check_min,
				check_max: s.check_max,
				nextword_min: s.nextword_min,
				nextword_max: s.nextword_max,
				mode: s.mode
			});
		}
	});
}

chrome.storage.onChanged.addListener(function(changes, namespace) {
	loadSettings();
});
