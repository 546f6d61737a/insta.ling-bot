 /**
  * Plik domyslnych ustawien
  */
 
const _storage_code = 1;
/**
 * 0 Local
 * 1 Sync
 * 2 Managed
 */


var _mode = 1;
/**
 * 0x1 autocomplete
 * 0x2 from audio file name
 * 0x4 automode
 * 0x8 mute
 */

var _check_minTimeout = 5000;
var _check_maxTimeout = 30000;
var _nextWord_minTimeout = 3000;
var _nextWord_maxTimeout = 7000;

/**
 * timeout w milisekundach
 */


